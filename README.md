# Userstyles

This repository is a collection of my [UserCSS](https://github.com/openstyles/stylus/wiki/Usercss) styles for use with the [Stylus](https://add0n.com/stylus.html) browser extension.

### Prerequisites

The prerequisites to using the styles contained within this repository:

1. Have any of the following browsers installed: Firefox, Opera, Chrome
2. Have the [Stylus](https://add0n.com/stylus.html) browser extention installed from the relevant browsers addon/extension service - direct links available at [https://add0n.com/stylus.html](https://add0n.com/stylus.html).

### Usage

1. Simply press the "Install directly with Stylus" button on styles you want and the Stylus extension will install them.

##

# Styles:
##

## Anandtech.com - [Dark]  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/anandtech/anandtech-dark.user.css)

A dark themed restyling of TheRegister.com, a tech news website.

![Anandtech Dark Screenshot](anandtech/anandtech-dark.png "Anandtech Dark Screenshot")

##

## ArsTechnica - [Dark]  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/arstechnica/arstechnica-dark.user.css)

A dark themed restyling of ArsTechnica.com, a tech news website.

![ArsTechnica Dark Screenshot](arstechnica/arstechnica-dark.png "ArsTechnica Dark Screenshot")

##

## Hacker News - [Dark] [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/hacker-news/hacker-news-dark.user.css)

A dark themed restyling of the news.ycombinator.com Hacker News portal

![Hacker News Dark Screenshot](hacker-news/hacker-news-dark.png "Hacker News Dark Screenshot")

##

## Phoronix - [Dark]  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/phoronix/phoronix-dark.user.css)

A dark themed restyling of the Phoronix Linux news website and forum threads

![Phoronix Dark Screenshot](phoronix/phoronix-dark.png "ArsTechnica Dark Screenshot")

##

## RPiLocator.com - [Dark]  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/rpilocator/rpilocator-dark.user.css)

A dark themed restyling of rpilocator.com's bootstrap theme

![RPiLocator Dark Screenshot](rpilocator/rpilocator-dark.png "RpiLocator Dark Mode Screenshot")

## 

## ScottishParliamentTV - Video Resize  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/scotparltv/scotparltv.user.css)

A restyling of the page to increase the size of the tiny video panel

![ScottishParliamentTV Dark Screenshot](scotparltv/scotparltv.png "ScottishParliamentTV Screenshot")

##

## TheRegister.com - [Dark]  [![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-00adad.svg)](https://gitlab.com/uro666/userstyles/-/raw/main/theregister/theregister-dark.user.css)

A dark themed restyling of TheRegister.com, a tech news website.

![TheRegister Dark Screenshot](theregister/theregister-dark.png "TheRegister Darkened Screenshot")

##
